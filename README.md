gamesrc-ver-recreation was converted to git, split into submodules and then
moved to a new location: https://bitbucket.org/gamesrc-ver-recreation/

Please see gamesrc-ver-recreation-hg-log.txt for reference
revision numbers and hashes from the Mercurial repository.
